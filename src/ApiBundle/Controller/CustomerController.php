<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use CoreBundle\Entity\Customer;
use CoreBundle\Entity\Product;

class CustomerController extends FOSRestController
{
     /**
     * @Rest\Get("/customer")
     */
    public function getAction(Request $req)
    {
        $logger = $this->get('logger');
        $logger->info('Request: "' . $req);

        $restresult = $this->getDoctrine()->getRepository('CoreBundle:Customer')->findAll();
        if ($restresult === null) {
          throw new HttpException(404, "No Customer Found");
        }
        
    

        return $restresult;
    }

     /**
	 * @Rest\Get("/customer/{id}")
	 */
	 public function idAction($id)
	 {
	   $singleresult = $this->getDoctrine()->getRepository('CoreBundle:Customer')->find($id);
	   if ($singleresult === null) {
	       throw new HttpException(404, "Cannot find customer");
	   }
	 return $singleresult;
	 }

    /**
     * @Rest\Post("/customer/")
     */
     public function postAction(Request $request)
     {
      
       $customer = new Customer();
       $fname = $request->get('firstName');
       $lname = $request->get('lastName');
       if(empty($fname) || empty($lname)){
        throw new HttpException(500, "First name and Last name is required"); 
       }
       $status = $request->get('status');
       $dob = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
       $createdAt = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
       $updatedAt = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
       $deletedAt = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
    
      $customer->setFirstName($fname);
      $customer->setLastName($lname);
      (!empty($status))?$customer->setStatus($status):'';
          $customer->setDateOfBirth($dob);
          $customer->setCreatedAt($createdAt);
          $customer->setUpdatedAt($updatedAt);
          $customer->setDeletedAt($deletedAt);
          $em = $this->getDoctrine()->getManager();
          $em->persist($customer);
          $em->flush();
          throw new HttpException(200, "Customer has been added Successfully");
     }


     /**
     * @Rest\Put("/customer/{id}")
     */
     public function updateAction($id,Request $request){
        $em = $this->getDoctrine()->getManager();
        $fname = $request->get('firstName');
        $lname = $request->get('lastName');
        $updatedAt = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
        $customer = $this->getDoctrine()->getRepository('CoreBundle:Customer')->find($id);
         if (empty($customer)) {
            throw new HttpException(404, "Cannot find customer");
         } 
         elseif(!empty($fname) && !empty($lname)){
           $customer->setFirstName($fname);
           $customer->setLastName($lname);
           $customer->setUpdatedAt($updatedAt);
           $em->flush();
           return new View("Customer Updated Successfully", Response::HTTP_OK);
         }elseif(empty($fname) && !empty($lname)){
           $customer->setUpdatedAt($updatedAt);
           $customer->setLastName($lname);
           $em->flush();
           throw new HttpException(200, "Customer last name has been updated Successfully");
        }
        elseif(!empty($fname) && empty($lname)){
            $customer->setUpdatedAt($updatedAt);
            $customer->setFirstName($fname);
            $em->flush();
            throw new HttpException(200, "Customer first name has been updated Successfully"); 
        }
        else throw new HttpException(500, "First name and Last name is required"); 
         }
       

     /**
     * @Rest\Delete("/customer/{id}")
     */
     public function deleteIdAction($id)
     {
      
        $sn = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository('CoreBundle:Customer')->find($id);
        if (empty($user)) {
          throw new HttpException(404, "Cannot find customer");
         }
         else {
          $sn->remove($user);
          $sn->flush();
         }
         return $user;
     }

}

