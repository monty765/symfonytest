<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use CoreBundle\Customer;
use CoreBundle\Entity\Product;

class ProductController extends FOSRestController
{
     
	 /**
     * @Rest\Get("/product")
     */
    public function getAction()
    {
      $restresult = $this->getDoctrine()->getRepository('CoreBundle:Product')->findAll();
        if ($restresult === null) {
          throw new HttpException(404, "No products found");
     }
        return $restresult;
    }

     /**
	 * @Rest\Get("/product/{id}")
	 */
	 public function idAction($id)
	 {
	   $singleresult = $this->getDoctrine()->getRepository('CoreBundle:Product')->find($id);
	   if ($singleresult === null) {
	   			throw new HttpException(404, "Cannot find product");
	   }
	 	return $singleresult;
	 }

	 /**
     * @Rest\Post("/product/")
     */
     public function postAction(Request $request)
     {
      $em = $this->getDoctrine()->getManager();
       $product = new Product();
	   $name = $request->get('name');
	   $status = $request->get('status');
	   $customerId = $request->get('customerId');
	   $createdAt = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
	   $updatedAt = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
	   $deletedAt = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
	 if(empty($name))
	 {
	   throw new HttpException(500, "Product name is required"); 
	 } 
	 $customer = $this->getDoctrine()->getRepository('CoreBundle:Customer')->find($id);
	  $product->setName($name);
	  $product->setStatus($status);
	  $product->setCustomer($customer);
	  $product->setCreatedAt($createdAt);
	  $product->setUpdatedAt($updatedAt);
	  $product->setDeletedAt($deletedAt );
	  return $product;
	  $em->persist($product);
	  $em->flush();
	   return new View("User Added Successfully", Response::HTTP_OK);
     }

	 /**
     * @Rest\Delete("/product/{id}")
     */
     public function deleteIdAction($id)
     {
      
        $em = $this->getDoctrine()->getManager();
        $product = $this->getDoctrine()->getRepository('CoreBundle:Product')->find($id);
        if (empty($product)) {
          	throw new HttpException(404, "Cannot find product");
         }
         else {
          $em->remove($product);
          $em->flush();
         }
         return $product;
     }
}

