<?php

namespace CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $status = array(
            'New' => 'New',
            'Pending' => 'Pending',
            'In Review' => 'In Review',
            'Approved' => 'Approved',
            'Inactive' => 'Inactive',
            'Deleted' => 'Deleted'
            );

        $builder->add('name', TextType::class, [
            'required'    => true,
            'constraints' =>
            [
            new NotBlank()
            ]
            ])
        ->add('status',ChoiceType::class, array(
            'choices' => $status
            ))       
        ->add('customer', EntityType::class, [
            'required' => true,
            'class'    => 'CoreBundle\Entity\Customer'
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\Product'
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'corebundle_product';
    }


}
