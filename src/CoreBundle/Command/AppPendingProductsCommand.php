<?php

namespace CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class AppPendingProductsCommand extends ContainerAwareCommand
{
  protected function configure()
  {
    $this
    ->setName('app:pending-products')
    ->setDescription('...')
    ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
    ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
   $manager = $this->getContainer()->get('doctrine.orm.entity_manager');
   $productRepo = $manager->getRepository('CoreBundle:Product');
   $products = $productRepo->findByStatus('new');
   $bar = New ProgressBar($output, count($products));
   $bar->start();
   foreach ($products as $key => $value) {
     $output->writeln($value->getName());
     $bar->advance();
   }
   $bar->finish();
   $message = (new \Swift_Message('Hello Email'))
   ->setFrom('myapp@example.com')
   ->setTo('mahanteshiremat@gmail.com')
   ->setBody(
    $this->getContainer()->get('templating')->render(
      'Emails/pendingProducts.html.twig',
      array('products' => $products)
      ),
    'text/html'
    );

   $this->getContainer()->get('mailer')->send($message);
   $output->writeln('Pending Products has been sent to mahanteshiremat@gmail.com');
 }
 

}
