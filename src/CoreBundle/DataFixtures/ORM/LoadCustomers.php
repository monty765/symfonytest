<?php


namespace CoreBundle\DataFixtures\ORM;

use CoreBundle\Entity\Customer;
use CoreBundle\Entity\Product;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCustomers extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $date = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
        $customer = new Customer();
        $customer->setFirstName('Customer1');
        $customer->setLastName('Last Name1');
        $customer->setDateOfBirth($date);
        $customer->setCreatedAt($date);

        $customer1 = new Customer();
        $customer1->setFirstName('Customer2');
        $customer1->setLastName('Last Name2');
        $customer1->setDateOfBirth($date);
        $customer1->setCreatedAt($date);

        $manager->persist($customer);
        $manager->persist($customer1);
        $manager->flush();
        $this->addReference("Customer", $customer);
        $this->addReference("Customer1", $customer1);
        
    }


    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}