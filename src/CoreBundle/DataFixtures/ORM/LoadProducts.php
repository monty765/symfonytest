<?php

namespace CoreBundle\DataFixtures\ORM;

use CoreBundle\Entity\Product;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadProducts extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	$date = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
        $product = new Product();
        $product->setName('Product 1');
        $product->setCreatedAt($date);
        $product->setCustomer($this->getReference("Customer"));


        $product1 = new Product();
        $product1->setName('Product 2');
        $product1->setStatus('pending');
        $product1->setCreatedAt($date);
        $product1->setCustomer($this->getReference("Customer1"));

        $manager->persist($product);
        $manager->persist($product1);
        $manager->flush();
    }


    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}